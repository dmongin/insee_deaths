# 2020 excess deaths in France

Here is an R analysis of the excess death data in France.
The raw deaths data re available at the French national institute of statistics: https://www.data.gouv.fr/es/datasets/fichier-des-personnes-decedees/#_ , which gives the nominative list of all french person dead since 1970 (!!). 

The population statistiques per age and sex category are available here: https://www.insee.fr/fr/statistiques/pyramide/3312958/xls/pyramides-des-ages_bilan-demo_2019.xls

The file [get_death_data.R](./get_death_data.R) allow to download the data, parse and clean them, create an SQlite database (~2Go), and extract from it the number of deaths per day, age and sex since 1970 in France (death_info_1970-2020.csv, created by the script).
The script create a log.txt file, giving various logging infos (the whole data downloading and cleaning can take ~2h).

The file [analysis.R](./analysis.R) allows to perform basic statistics and create graphs using ggplot:

![deaths per day](./figures/death_excess_en.png)


![deaths in percent of population](./figures/death_excesspcpop_en.png)


![deaths age pyramide](./figures/death_pyramide_en.png)






